# Representation Theory Exercises

Course by Alexander Yom Din, spring of 2023, the Hebrew University of Jerusalem, Israel.

[Ex 1](https://omryco.gitlab.io/representation-theory-exercises/ex1.pdf)

[Ex 2](https://omryco.gitlab.io/representation-theory-exercises/ex2.pdf)

[Ex 3](https://omryco.gitlab.io/representation-theory-exercises/ex3.pdf)

[Ex 5](https://omryco.gitlab.io/representation-theory-exercises/ex5.pdf)

[Ex 6](https://omryco.gitlab.io/representation-theory-exercises/ex6.pdf)

[Ex 7](https://omryco.gitlab.io/representation-theory-exercises/ex7.pdf)

[Ex 8](https://omryco.gitlab.io/representation-theory-exercises/ex8.pdf)
