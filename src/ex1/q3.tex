\section{}

\subsection{}

Let us show that the mapping

\[
    \psi : G \to G^\text{op} \\
    \psi(g) = (g^{-1})^\text{op}
\]

is an isomorphism. It is a group homomorphism since

\[
    \psi(1) = (1^{-1})^\text{op} = 1^\text{op} \\
    \psi(g)\psi(h) = (g^{-1})^\text{op} (h^{-1})^\text{op}
    = (h^{-1}g^{-1})^\text{op} = ((gh)^{-1})^\text{op} = \psi(gh)
\]

it is onto since \(\psi(h^{-1}) = h^\text{op}\), and it is injective
since if \(\psi(g)=\psi(h)\) so \(\psi(gh^{-1})=1^\text{op}\) so
\(1=(gh^{-1})^{-1}=hg^{-1}\) so \(g=h\).

\subsection{}

We have showed that \(G\) can left act on itself by \(g\cdot x=gx\).
Similarily, it can right act on itself by \(x\cdot g=xg\). Restricting
the left action to \(H\) and the right action to \(K\), we obtain the
desired structure, as these two actions manifestly commute.

\subsection{}

Let \(X\) be a \((G,H)\)-set. So by construction, \(G\times_G X\) is
also a \((G,H)\)-set.

Let us define a mapping

\[
    \psi: G\times_GX\to X \\
    \psi(g,x) = g\cdot x
\]

First, let us show that it is well defined. If \((a,b)=(c,d)\), so there
exists \(g\in G\) such that \(a=c\cdot g\) and \(g\cdot b=d\). So

\[
    \psi(a,b) = a\cdot b = (c\cdot g)\cdot b =
    c\cdot(g\cdot b) = c\cdot d = \psi(c,d)
\]

It is \((G,H)\) equivariant since since for some \(g\in G\) and
\(h\in H\) it holds that

\[
    \psi(g\cdot(a,b)\cdot h) = \psi(g\cdot a, b\cdot h) =
    (g\cdot a) \cdot (b\cdot h) = \\
    g\cdot (a \cdot b)\cdot h =
    g\cdot\psi(a,b)\cdot h
\]

It is surjective since \(\psi(1,x)=1\cdot x=x\), and it is injective
since if \(\psi(g,x)=\psi(h,y)\) so \(g\cdot x=h\cdot y\), so
\(l=h^{-1}g\) is such that \(g=h\cdot l\) and \(l\cdot x=y\) meaning
that \((g,x)=(h,y)\). Therefore \(\psi\) is an isomorphism of
\((G,H)\)-sets.

\subsection{}

Let us define a mapping

\[
    \psi: \text{pt}\times_GX\to X \\
    \psi(1,x) = \text{Orb}(x)
\]

First, let us show that it is well defined. If \((1,x)=(1,y)\), so there
exists \(g\in G\) such that \(1=1\cdot g\) (since \(G\) acts trivially
on \(\text{pt}\)) and \(g\cdot x=y\). Therefore \(x,y\) are in the same
orbit so \(\text{Orb}(x)=\text{Orb}(y)\).

It is \((G,1)\) equivariant since since for some \(g\in G\) it holds
that

\[
    \psi(g\cdot1,x) = \psi(1,x) = \text{Orb}(x) =
    g\cdot\text{Orb}(x) = g\cdot\psi(1,x)
\]

It is surjective since \(\psi(1,x)=\text{Orb}(x)\), and it is injective
since if \(\psi(1,x)=\psi(1,y)\) so \(\text{Orb}(x)=\text{Orb}(y)\), so
there exists \(g\in G\) such that \(g\cdot x=y\) so

\[
    (1,x) = (1\cdot g, x) = (1, g\cdot x) = (1,y)
\]

Therefore \(\psi\) is an isomorphism of \((G,1)\) sets.

\subsection{}

Let \(\psi\in\Hom_G(G\times_HX, Y)\). Let us define
\(\phi\in\Hom_H(X,Y|_H)\) such that

\[
    \phi(x) = \psi(1,x)
\]

The mapping \(\phi\) is indeed \(H\)-equivariant, since for \(h\in H\)
it holds that

\[
    \phi(h\cdot x) = \psi(1,h\cdot x) = \psi(1\cdot h,x) = \\
    \psi(h,x) = \psi(h\cdot1,x) = h\cdot\psi(1,x) =
    h\cdot\phi(x)
\]

Let us define

\[
    \mathcal L : \Hom_G(G\times_HX, Y) \to \in\Hom_H(X,Y|_H) \\
    \mathcal L[\psi]=\phi
\]

So we wish to show that \(\mathcal L\) is a bijection.

Let \(\psi,\psi^\prime\in\Hom_G(G\times_HX,Y)\) such that
\(\mathcal L[\psi]=\mathcal L[\psi^\prime]\). So
\(\psi(1,x)=\psi^\prime(1,x)\) for all \(x\in X\). Therefore

\[
    \psi(g,x) = \psi(g\cdot1, x) = g\cdot\psi(1, x) = \\
    g\cdot\psi^\prime(1, x) = \psi^\prime(g\cdot1, x) = \psi^\prime(g, x)
\]

so \(\psi=\psi^\prime\), meaning that \(\mathcal L\) is injective.

Let \(\phi\in\Hom_H(X,Y|_H)\). Let us define
\(\psi\in \Hom_G(G\times_HX,Y)\) such that
\(\psi(g,x) = g\cdot\phi(x)\). First, let us show that it is well
defined. For \((g,x)=(g^\prime,x^\prime)\) there exists \(h\in H\) such
that \(g=g^\prime\cdot h\) and \(h\cdot x=g^\prime\). So

\[
    \psi(g,x) = g\cdot\phi(x) = gh^{-1}\cdot\phi(hx)=
    g^\prime\cdot\phi(g^\prime) = \psi(g^\prime, x^\prime)
\]

Second, let us show that it is indeed \(G\)-equivariant. Let \(g\in G\),
so

\[
    \psi(g\cdot a, b) = (g\cdot a) \cdot \phi(b) =
    g\cdot(a\cdot\phi(b)) = g\cdot\psi(a,b)
\]

Third, it holds that \(\psi(1,x)=1\cdot\phi(x)=\phi(x)\) for every
\(x\in X\), meaning that \(\mathcal L[\psi]=\phi\), so \(\mathcal L\) is
also surjective. Overall

\[
    \Hom_G(G\times_HX, Y) \cong \in\Hom_H(X,Y|_H)
\]

Choosing \(H=1\), we get that

\[
    \Hom_G(G\times X, Y) \cong \in\Hom_{\Set}(X,Y)
\]
